const tabs = document.querySelectorAll('.tabs li');
const content = document.querySelectorAll('.tabs-content li');
for (let i = 0; i < tabs.length; i++) {
    (function(i) {
        const tab = tabs[i];
        tab.onclick = function () {
            for(let j=0; j <content.length; j++) {
                let opacity = window.getComputedStyle(content[j]).opacity;
                if(opacity === "1") {
                    content[j].style.opacity = "0";
                }
            }
            content[i].style.opacity = "1";
        }
    })(i);
}